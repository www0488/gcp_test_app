const express = require('express');
const Firestore = require('@google-cloud/firestore');

const app = express();

const db = new Firestore({
  projectId: 'testapp-245014',
  keyFilename: './testApp-ff470d3d1287.json'
});

const collection = db.collection('customers');

// Create Connection

app.get('/customers/:id', (req, res) => {
  const id = req.params.id;

  collection
    .doc(id)
    .get()
    .then((customer) => {
      res.send(JSON.stringify(customer.data()));
    })
    .catch((error) => {
      console.log(error);
    })
});

app.get('/customers', (req, res) => {
  let customers = [];
  collection
    .get()
    .then((result) => {
      result.forEach((doc) => {
        customers.push(doc.data());
      });

      res.send(JSON.stringify(customers));
    })
    .catch((error) => {
      console.log(error);
      throw error;
    });
});

app.get('/', (req, res) => {
  res.send('Välkommen.');
});

app.listen('8080', () => {
    console.log('Server started on port 8080');
});

